import { combineReducers } from 'redux';
import AppReducer from './AppModule/AppReducer';

const rootReducer = combineReducers({
  App: AppReducer
});

export default rootReducer;
