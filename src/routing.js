import React from 'react';
import {Route, Redirect} from 'react-router-dom';
import App from './AppModule/App';
import Login from './AuthModule/Login/Login'

export const Routes = () =>(
  <div>
    <PrivateRoute exact path='/' component={App}/>
    <Route path='/login' component={Login}/>
  </div>
)

const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props =>
      true ? (
        <Component {...props} />
      ) : (
        <Redirect
          to={{
            pathname: "/login",
            state: { from: props.location }
          }}
        />
      )
    }
  />
);