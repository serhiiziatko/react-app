import React from 'react';
import ReactDOM from 'react-dom';
import registerServiceWorker from './registerServiceWorker';
import {Provider} from "react-redux";
import store from './rootStore';
import {BrowserRouter} from 'react-router-dom';
import {Routes} from './routing';
import './index.css';
import './SharedModule/reset.css';

ReactDOM.render(
    <Provider store={store}>
      <BrowserRouter>
        <Routes />
      </BrowserRouter>
    </Provider>, 
    document.getElementById('root')
);
registerServiceWorker();
